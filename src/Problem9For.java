import java.util.Scanner;

public class Problem9For {
	public static void main(String[] args) {
		String num;
		int n;
		Scanner sc = new Scanner(System.in);
		System.out.print("Please input n: ");
		num = sc.next();
		n = Integer.parseInt(num);
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				System.out.print(j+1);
			}
			System.out.println();
		}
	}
}
