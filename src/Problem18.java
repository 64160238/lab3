import java.util.Scanner;

public class Problem18 {
	public static void main(String[] args) {
		String type;
		String number;
		int Type;
		int Number;
		Scanner sc = new Scanner(System.in);
		System.out.print("Please select star type [1-4,5 is Exit]: ");
		type = sc.next();
		System.out.print("Please input number: ");
		number = sc.next();
		Type = Integer.parseInt(type);
		Number = Integer.parseInt(number);
		if (Type == 1) {
			for (int i = 1; i <= Number; i++) {
				for(int j = 0; j < i; j++){
					System.out.print("*");
				}
			System.out.println();
			}
		}else if (Type == 2) {
			for (int i = Number; i >= 1; i--) {
				for(int j = 0; j < i; j++){
					System.out.print("*");
				}
			System.out.println();
			}
		}else if (Type == 3) {
			for (int i = 0; i < Number; i++) {
				for(int j = 0; j < Number; j++){
					if(j>=i) {
						System.out.print("*");
					}else {
						System.out.print(" ");
					}
				}
				System.out.println();
			}
		}else if (Type == 4){
			for (int i = 4; i >= 0; i--) {
				for(int j = 0; j < Number; j++){
					if(j>=i) {
						System.out.print("*");
					}else {
						System.out.print(" ");
					}
				}
				System.out.println();
			}
		}else if (Type == 5) {
			System.out.print("Bye bye!!!");
		}else{
			System.out.println("Error: Please input number between 1-5");
		}
	}
}
